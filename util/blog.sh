#!/bin/sh
DIRNAME=`basename "${1}" .svg | tr "_" "-"`
DEST="../../../content/portfolio/${DIRNAME}.md"

echo "processing ${1}"

echo "+++" > $DEST
echo "showonlyimage = true" >> $DEST
echo "draft = false" >> $DEST
echo 'image = "img/'${1}'"' >> $DEST
echo 'date = "2019-11-07T21:12:22+05:30"' >> $DEST
echo 'title = "'"${DIRNAME}"'"' >> $DEST
echo "weight = 0" >> $DEST
echo "+++" >> $DEST
printf "\n"  >> $DEST
echo '![Rose](/img/'${1}')' >> $DEST
printf "\n" >> $DEST
echo '[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)' >> $DEST

