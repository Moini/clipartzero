+++
date = "2019-11-07T13:13:13+05:30"
title = "Get in touch"
+++

Clipart Zero is committed to being collaborative and open. If you want to contribute, get in touch.
