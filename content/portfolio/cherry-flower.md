+++
showonlyimage = true
draft = false
image = "img/cherry_flower.svg"
date = "2019-01-01T21:12:22+05:30"
title = "cherry-flower"
weight = 0
+++

![Right-click to Save image](/img/cherry_flower.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
