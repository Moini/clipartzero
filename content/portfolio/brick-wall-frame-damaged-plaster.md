+++
showonlyimage = true
draft = false
image = "img/brick_wall_frame_damaged_plaster.svg"
date = "2019-01-01T21:12:22+05:30"
title = "brick-wall-frame-damaged-plaster"
weight = 0
+++

![Right-click to Save image](/img/brick_wall_frame_damaged_plaster.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
