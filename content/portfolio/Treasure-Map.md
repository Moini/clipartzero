+++
showonlyimage = true
draft = false
image = "img/Treasure_Map.svg"
date = "2019-01-01T21:12:22+05:30"
title = "Treasure-Map"
weight = 0
+++

![Right-click to Save image](/img/Treasure_Map.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
