+++
showonlyimage = true
draft = false
image = "img/clipart-scissors-oc.svg"
date = "2019-01-01T21:12:22+05:30"
title = "clipart-scissors-oc"
weight = 0
+++

![Right-click to Save image](/img/clipart-scissors-oc.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
