+++
showonlyimage = true
draft = false
image = "img/LimeTreeLowres.svg"
date = "2019-01-01T21:12:22+05:30"
title = "LimeTreeLowres"
weight = 0
+++

![Right-click to Save image](/img/LimeTreeLowres.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
