+++
showonlyimage = true
draft = false
image = "img/Juv_Cedar_Waxwing.svg"
date = "2019-01-01T21:12:22+05:30"
title = "Juv-Cedar-Waxwing"
weight = 0
+++

![Right-click to Save image](/img/Juv_Cedar_Waxwing.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
