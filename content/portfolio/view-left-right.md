+++
showonlyimage = true
draft = false
image = "img/view_left_right.svg"
date = "2019-01-01T21:12:22+05:30"
title = "view-left-right"
weight = 0
+++

![Right-click to Save image](/img/view_left_right.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
