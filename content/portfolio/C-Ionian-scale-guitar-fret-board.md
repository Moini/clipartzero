+++
showonlyimage = true
draft = false
image = "img/C-Ionian-scale-guitar-fret-board.svg"
date = "2019-01-01T21:12:22+05:30"
title = "C-Ionian-scale-guitar-fret-board"
weight = 0
+++

![Right-click to Save image](/img/C-Ionian-scale-guitar-fret-board.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
