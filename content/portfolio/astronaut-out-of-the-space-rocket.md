+++
showonlyimage = true
draft = false
image = "img/astronaut_out_of_the_space_rocket.svg"
date = "2019-01-01T21:12:22+05:30"
title = "astronaut-out-of-the-space-rocket"
weight = 0
+++

![Right-click to Save image](/img/astronaut_out_of_the_space_rocket.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
