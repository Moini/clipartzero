+++
showonlyimage = true
draft = false
image = "img/58294main_The.Brain.in.Space-page-56-telescope-bare.svg"
date = "2019-01-01T21:12:22+05:30"
title = "58294main-The.Brain.in.Space-page-56-telescope-bare"
weight = 0
+++

![Right-click to Save image](/img/58294main_The.Brain.in.Space-page-56-telescope-bare.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
