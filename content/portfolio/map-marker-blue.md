+++
showonlyimage = true
draft = false
image = "img/map_marker_blue.svg"
date = "2019-01-01T21:12:22+05:30"
title = "map-marker-blue"
weight = 0
+++

![Right-click to Save image](/img/map_marker_blue.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
