+++
showonlyimage = true
draft = false
image = "img/Political-Map-Earth-Cylinder.svg"
date = "2019-01-01T21:12:22+05:30"
title = "Political-Map-Earth-Cylinder"
weight = 0
+++

![Right-click to Save image](/img/Political-Map-Earth-Cylinder.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
