+++
showonlyimage = true
draft = false
image = "img/preferences-desktop-keyboard-shortcuts.svg"
date = "2019-01-01T21:12:22+05:30"
title = "preferences-desktop-keyboard-shortcuts"
weight = 0
+++

![Right-click to Save image](/img/preferences-desktop-keyboard-shortcuts.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
