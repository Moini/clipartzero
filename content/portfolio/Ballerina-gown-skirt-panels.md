+++
showonlyimage = true
draft = false
image = "img/Ballerina_gown_skirt_panels.svg"
date = "2019-01-01T21:12:22+05:30"
title = "Ballerina-gown-skirt-panels"
weight = 0
+++

![Right-click to Save image](/img/Ballerina_gown_skirt_panels.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
