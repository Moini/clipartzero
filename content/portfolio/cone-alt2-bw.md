+++
showonlyimage = true
draft = false
image = "img/cone_alt2_bw.svg"
date = "2019-01-01T21:12:22+05:30"
title = "cone-alt2-bw"
weight = 0
+++

![Right-click to Save image](/img/cone_alt2_bw.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
