+++
showonlyimage = true
draft = false
image = "img/johnny_automatic_flowering_tree.svg"
date = "2019-01-01T21:12:22+05:30"
title = "johnny-automatic-flowering-tree"
weight = 0
+++

![Right-click to Save image](/img/johnny_automatic_flowering_tree.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
