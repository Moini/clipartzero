+++
showonlyimage = true
draft = false
image = "img/logo-openclipart--akramly.svg"
date = "2019-01-01T21:12:22+05:30"
title = "logo-openclipart--akramly"
weight = 0
+++

![Right-click to Save image](/img/logo-openclipart--akramly.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
