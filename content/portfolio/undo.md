+++
showonlyimage = true
draft = false
image = "img/undo.svg"
date = "2019-01-01T21:12:22+05:30"
title = "undo"
weight = 0
+++

![Right-click to Save image](/img/undo.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
