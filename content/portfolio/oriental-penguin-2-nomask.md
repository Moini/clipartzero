+++
showonlyimage = true
draft = false
image = "img/oriental_penguin_2_nomask.svg"
date = "2019-01-01T21:12:22+05:30"
title = "oriental-penguin-2-nomask"
weight = 0
+++

![Right-click to Save image](/img/oriental_penguin_2_nomask.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
