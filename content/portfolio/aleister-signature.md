+++
showonlyimage = true
draft = false
image = "img/aleister-signature.svg"
date = "2019-01-01T21:12:22+05:30"
title = "aleister-signature"
weight = 0
+++

![Right-click to Save image](/img/aleister-signature.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
