+++
showonlyimage = true
draft = false
image = "img/OC_Icon_bw.svg"
date = "2019-01-01T21:12:22+05:30"
title = "OC-Icon-bw"
weight = 0
+++

![Right-click to Save image](/img/OC_Icon_bw.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
