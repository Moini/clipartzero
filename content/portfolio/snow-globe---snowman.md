+++
showonlyimage = true
draft = false
image = "img/snow_globe_-_snowman.svg"
date = "2019-01-01T21:12:22+05:30"
title = "snow-globe---snowman"
weight = 0
+++

![Right-click to Save image](/img/snow_globe_-_snowman.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
