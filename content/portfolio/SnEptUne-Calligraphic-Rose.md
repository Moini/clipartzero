+++
showonlyimage = true
draft = false
image = "img/SnEptUne_Calligraphic_Rose.svg"
date = "2019-01-01T21:12:22+05:30"
title = "SnEptUne-Calligraphic-Rose"
weight = 0
+++

![Right-click to Save image](/img/SnEptUne_Calligraphic_Rose.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
