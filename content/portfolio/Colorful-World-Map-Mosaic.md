+++
showonlyimage = true
draft = false
image = "img/Colorful-World-Map-Mosaic.svg"
date = "2019-01-01T21:12:22+05:30"
title = "Colorful-World-Map-Mosaic"
weight = 0
+++

![Right-click to Save image](/img/Colorful-World-Map-Mosaic.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
