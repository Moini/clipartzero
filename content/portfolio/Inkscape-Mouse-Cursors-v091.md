+++
showonlyimage = true
draft = false
image = "img/Inkscape-Mouse-Cursors-v091.svg"
date = "2019-01-01T21:12:22+05:30"
title = "Inkscape-Mouse-Cursors-v091"
weight = 0
+++

![Right-click to Save image](/img/Inkscape-Mouse-Cursors-v091.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
