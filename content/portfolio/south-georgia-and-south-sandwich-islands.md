+++
showonlyimage = true
draft = false
image = "img/south_georgia_and_south_sandwich_islands.svg"
date = "2019-01-01T21:12:22+05:30"
title = "south-georgia-and-south-sandwich-islands"
weight = 0
+++

![Right-click to Save image](/img/south_georgia_and_south_sandwich_islands.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
