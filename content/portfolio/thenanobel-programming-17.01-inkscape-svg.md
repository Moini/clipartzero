+++
showonlyimage = true
draft = false
image = "img/thenanobel-programming-17.01-inkscape-svg.svg"
date = "2019-01-01T21:12:22+05:30"
title = "thenanobel-programming-17.01-inkscape-svg"
weight = 0
+++

![Right-click to Save image](/img/thenanobel-programming-17.01-inkscape-svg.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
