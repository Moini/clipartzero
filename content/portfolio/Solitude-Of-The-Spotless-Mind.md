+++
showonlyimage = true
draft = false
image = "img/Solitude-Of-The-Spotless-Mind.svg"
date = "2019-01-01T21:12:22+05:30"
title = "Solitude-Of-The-Spotless-Mind"
weight = 0
+++

![Right-click to Save image](/img/Solitude-Of-The-Spotless-Mind.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
