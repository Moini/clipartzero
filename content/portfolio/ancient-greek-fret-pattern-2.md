+++
showonlyimage = true
draft = false
image = "img/ancient_greek_fret_pattern_2.svg"
date = "2019-01-01T21:12:22+05:30"
title = "ancient-greek-fret-pattern-2"
weight = 0
+++

![Right-click to Save image](/img/ancient_greek_fret_pattern_2.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
