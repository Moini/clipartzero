+++
showonlyimage = true
draft = false
image = "img/Power-Cable-Icon-Redrawn.svg"
date = "2019-01-01T21:12:22+05:30"
title = "Power-Cable-Icon-Redrawn"
weight = 0
+++

![Right-click to Save image](/img/Power-Cable-Icon-Redrawn.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
