+++
showonlyimage = true
draft = false
image = "img/led_triangular_1_blue.svg"
date = "2019-01-01T21:12:22+05:30"
title = "led-triangular-1-blue"
weight = 0
+++

![Right-click to Save image](/img/led_triangular_1_blue.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
