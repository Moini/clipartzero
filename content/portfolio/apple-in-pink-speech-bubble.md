+++
showonlyimage = true
draft = false
image = "img/apple-in-pink-speech-bubble.svg"
date = "2019-01-01T21:12:22+05:30"
title = "apple-in-pink-speech-bubble"
weight = 0
+++

![Right-click to Save image](/img/apple-in-pink-speech-bubble.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
