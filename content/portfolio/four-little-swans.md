+++
showonlyimage = true
draft = false
image = "img/four-little-swans.svg"
date = "2019-01-01T21:12:22+05:30"
title = "four-little-swans"
weight = 0
+++

![Right-click to Save image](/img/four-little-swans.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
