+++
showonlyimage = true
draft = false
image = "img/Book_thick_generic.svg"
date = "2019-01-01T21:12:22+05:30"
title = "Book-thick-generic"
weight = 0
+++

![Right-click to Save image](/img/Book_thick_generic.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
