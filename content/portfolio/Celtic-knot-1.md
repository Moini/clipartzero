+++
showonlyimage = true
draft = false
image = "img/Celtic_knot_1.svg"
date = "2019-01-01T21:12:22+05:30"
title = "Celtic-knot-1"
weight = 0
+++

![Right-click to Save image](/img/Celtic_knot_1.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
