+++
showonlyimage = true
draft = false
image = "img/Abstract_Shapes_With_Patterns.svg"
date = "2019-01-01T21:12:22+05:30"
title = "Abstract-Shapes-With-Patterns"
weight = 0
+++

![Right-click to Save image](/img/Abstract_Shapes_With_Patterns.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
