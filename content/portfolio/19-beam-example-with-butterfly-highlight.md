+++
showonlyimage = true
draft = false
image = "img/19_beam_example_with_butterfly_highlight.svg"
date = "2019-01-01T21:12:22+05:30"
title = "19-beam-example-with-butterfly-highlight"
weight = 0
+++

![Right-click to Save image](/img/19_beam_example_with_butterfly_highlight.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
