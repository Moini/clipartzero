+++
showonlyimage = true
draft = false
image = "img/Israel-and-Disputed-Territories.svg"
date = "2019-01-01T21:12:22+05:30"
title = "Israel-and-Disputed-Territories"
weight = 0
+++

![Right-click to Save image](/img/Israel-and-Disputed-Territories.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
