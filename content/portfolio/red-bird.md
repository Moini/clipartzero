+++
showonlyimage = true
draft = false
image = "img/red_bird.svg"
date = "2019-01-01T21:12:22+05:30"
title = "red-bird"
weight = 0
+++

![Right-click to Save image](/img/red_bird.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
