+++
showonlyimage = true
draft = false
image = "img/Knallfrosch_plain_svg.svg"
date = "2019-01-01T21:12:22+05:30"
title = "Knallfrosch-plain-svg"
weight = 0
+++

![Right-click to Save image](/img/Knallfrosch_plain_svg.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
