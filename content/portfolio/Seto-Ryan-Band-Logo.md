+++
showonlyimage = true
draft = false
image = "img/Seto_Ryan_Band_Logo.svg"
date = "2019-01-01T21:12:22+05:30"
title = "Seto-Ryan-Band-Logo"
weight = 0
+++

![Right-click to Save image](/img/Seto_Ryan_Band_Logo.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
