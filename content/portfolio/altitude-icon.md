+++
showonlyimage = true
draft = false
image = "img/altitude_icon.svg"
date = "2019-01-01T21:12:22+05:30"
title = "altitude-icon"
weight = 0
+++

![Right-click to Save image](/img/altitude_icon.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
