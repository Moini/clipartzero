+++
showonlyimage = true
draft = false
image = "img/openclipart-big-scissors-childen.svg"
date = "2019-01-01T21:12:22+05:30"
title = "openclipart-big-scissors-childen"
weight = 0
+++

![Right-click to Save image](/img/openclipart-big-scissors-childen.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
