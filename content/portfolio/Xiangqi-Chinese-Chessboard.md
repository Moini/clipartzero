+++
showonlyimage = true
draft = false
image = "img/Xiangqi_Chinese_Chessboard.svg"
date = "2019-01-01T21:12:22+05:30"
title = "Xiangqi-Chinese-Chessboard"
weight = 0
+++

![Right-click to Save image](/img/Xiangqi_Chinese_Chessboard.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
