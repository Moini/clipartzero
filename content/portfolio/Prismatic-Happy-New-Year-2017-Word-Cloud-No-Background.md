+++
showonlyimage = true
draft = false
image = "img/Prismatic-Happy-New-Year-2017-Word-Cloud-No-Background.svg"
date = "2019-01-01T21:12:22+05:30"
title = "Prismatic-Happy-New-Year-2017-Word-Cloud-No-Background"
weight = 0
+++

![Right-click to Save image](/img/Prismatic-Happy-New-Year-2017-Word-Cloud-No-Background.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
