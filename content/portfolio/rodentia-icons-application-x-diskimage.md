+++
showonlyimage = true
draft = false
image = "img/rodentia-icons_application-x-diskimage.svg"
date = "2019-01-01T21:12:22+05:30"
title = "rodentia-icons-application-x-diskimage"
weight = 0
+++

![Right-click to Save image](/img/rodentia-icons_application-x-diskimage.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
