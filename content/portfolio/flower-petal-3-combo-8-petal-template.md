+++
showonlyimage = true
draft = false
image = "img/flower_petal_3_combo_8_petal_template.svg"
date = "2019-01-01T21:12:22+05:30"
title = "flower-petal-3-combo-8-petal-template"
weight = 0
+++

![Right-click to Save image](/img/flower_petal_3_combo_8_petal_template.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
