+++
showonlyimage = true
draft = false
image = "img/pelican-icon-white.svg"
date = "2019-01-01T21:12:22+05:30"
title = "pelican-icon-white"
weight = 0
+++

![Right-click to Save image](/img/pelican-icon-white.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
