+++
showonlyimage = true
draft = false
image = "img/Wenceslas-Hollar-Jesus-after-Leonardo.svg"
date = "2019-01-01T21:12:22+05:30"
title = "Wenceslas-Hollar-Jesus-after-Leonardo"
weight = 0
+++

![Right-click to Save image](/img/Wenceslas-Hollar-Jesus-after-Leonardo.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
