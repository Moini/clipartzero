+++
showonlyimage = true
draft = false
image = "img/Armistice-Day-France.svg"
date = "2019-01-01T21:12:22+05:30"
title = "Armistice-Day-France"
weight = 0
+++

![Right-click to Save image](/img/Armistice-Day-France.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
