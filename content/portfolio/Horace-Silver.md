+++
showonlyimage = true
draft = false
image = "img/Horace_Silver.svg"
date = "2019-01-01T21:12:22+05:30"
title = "Horace-Silver"
weight = 0
+++

![Right-click to Save image](/img/Horace_Silver.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
