+++
showonlyimage = true
draft = false
image = "img/18_4_beam_example_with_FFT.svg"
date = "2019-01-01T21:12:22+05:30"
title = "18-4-beam-example-with-FFT"
weight = 0
+++

![Right-click to Save image](/img/18_4_beam_example_with_FFT.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
