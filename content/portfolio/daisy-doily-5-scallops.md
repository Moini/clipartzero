+++
showonlyimage = true
draft = false
image = "img/daisy_doily_5_scallops.svg"
date = "2019-01-01T21:12:22+05:30"
title = "daisy-doily-5-scallops"
weight = 0
+++

![Right-click to Save image](/img/daisy_doily_5_scallops.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
