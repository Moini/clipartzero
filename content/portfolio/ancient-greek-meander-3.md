+++
showonlyimage = true
draft = false
image = "img/ancient_greek_meander_3.svg"
date = "2019-01-01T21:12:22+05:30"
title = "ancient-greek-meander-3"
weight = 0
+++

![Right-click to Save image](/img/ancient_greek_meander_3.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
