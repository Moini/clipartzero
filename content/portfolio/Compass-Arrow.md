+++
showonlyimage = true
draft = false
image = "img/Compass_Arrow.svg"
date = "2019-01-01T21:12:22+05:30"
title = "Compass-Arrow"
weight = 0
+++

![Right-click to Save image](/img/Compass_Arrow.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
