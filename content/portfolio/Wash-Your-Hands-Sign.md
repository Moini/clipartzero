+++
showonlyimage = true
draft = false
image = "img/Wash-Your-Hands-Sign.svg"
date = "2019-01-01T21:12:22+05:30"
title = "Wash-Your-Hands-Sign"
weight = 0
+++

![Right-click to Save image](/img/Wash-Your-Hands-Sign.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
