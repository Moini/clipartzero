+++
showonlyimage = true
draft = false
image = "img/Syria-Map-Flag-With-Palmyra.svg"
date = "2019-01-01T21:12:22+05:30"
title = "Syria-Map-Flag-With-Palmyra"
weight = 0
+++

![Right-click to Save image](/img/Syria-Map-Flag-With-Palmyra.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
