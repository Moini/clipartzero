+++
showonlyimage = true
draft = false
image = "img/Blue_Bulb_Icon.svg"
date = "2019-01-01T21:12:22+05:30"
title = "Blue-Bulb-Icon"
weight = 0
+++

![Right-click to Save image](/img/Blue_Bulb_Icon.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
