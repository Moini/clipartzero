+++
showonlyimage = true
draft = false
image = "img/moon_and_the_star.svg"
date = "2019-01-01T21:12:22+05:30"
title = "moon-and-the-star"
weight = 0
+++

![Right-click to Save image](/img/moon_and_the_star.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
