+++
showonlyimage = true
draft = false
image = "img/bolt_cutter.svg"
date = "2019-01-01T21:12:22+05:30"
title = "bolt-cutter"
weight = 0
+++

![Right-click to Save image](/img/bolt_cutter.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
