+++
showonlyimage = true
draft = false
image = "img/Tripode_de_Pintura_Inkscape_-_01.svg"
date = "2019-01-01T21:12:22+05:30"
title = "Tripode-de-Pintura-Inkscape---01"
weight = 0
+++

![Right-click to Save image](/img/Tripode_de_Pintura_Inkscape_-_01.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
