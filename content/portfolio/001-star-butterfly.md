+++
showonlyimage = true
draft = false
image = "img/001_star_butterfly.svg"
date = "2019-01-01T21:12:22+05:30"
title = "001-star-butterfly"
weight = 0
+++

![Right-click to Save image](/img/001_star_butterfly.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
