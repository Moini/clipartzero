+++
showonlyimage = true
draft = false
image = "img/Haeckel_Campanariae.svg"
date = "2019-01-01T21:12:22+05:30"
title = "Haeckel-Campanariae"
weight = 0
+++

![Right-click to Save image](/img/Haeckel_Campanariae.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
