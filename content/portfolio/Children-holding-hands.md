+++
showonlyimage = true
draft = false
image = "img/Children_holding_hands.svg"
date = "2019-01-01T21:12:22+05:30"
title = "Children-holding-hands"
weight = 0
+++

![Right-click to Save image](/img/Children_holding_hands.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
