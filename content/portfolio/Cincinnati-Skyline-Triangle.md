+++
showonlyimage = true
draft = false
image = "img/Cincinnati-Skyline-Triangle.svg"
date = "2019-01-01T21:12:22+05:30"
title = "Cincinnati-Skyline-Triangle"
weight = 0
+++

![Right-click to Save image](/img/Cincinnati-Skyline-Triangle.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
