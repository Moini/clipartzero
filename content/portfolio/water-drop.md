+++
showonlyimage = true
draft = false
image = "img/water_drop.svg"
date = "2019-01-01T21:12:22+05:30"
title = "water-drop"
weight = 0
+++

![Right-click to Save image](/img/water_drop.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
