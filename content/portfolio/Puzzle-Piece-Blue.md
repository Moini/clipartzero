+++
showonlyimage = true
draft = false
image = "img/Puzzle-Piece-Blue.svg"
date = "2019-01-01T21:12:22+05:30"
title = "Puzzle-Piece-Blue"
weight = 0
+++

![Right-click to Save image](/img/Puzzle-Piece-Blue.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
