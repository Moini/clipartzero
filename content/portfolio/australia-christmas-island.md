+++
showonlyimage = true
draft = false
image = "img/australia_christmas_island.svg"
date = "2019-01-01T21:12:22+05:30"
title = "australia-christmas-island"
weight = 0
+++

![Right-click to Save image](/img/australia_christmas_island.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
