+++
showonlyimage = true
draft = false
image = "img/Prismatic-Tiled-Drums-Set-Silhouette.svg"
date = "2019-01-01T21:12:22+05:30"
title = "Prismatic-Tiled-Drums-Set-Silhouette"
weight = 0
+++

![Right-click to Save image](/img/Prismatic-Tiled-Drums-Set-Silhouette.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
