+++
showonlyimage = true
draft = false
image = "img/chocolate_chip_cookie_stipple.svg"
date = "2019-01-01T21:12:22+05:30"
title = "chocolate-chip-cookie-stipple"
weight = 0
+++

![Right-click to Save image](/img/chocolate_chip_cookie_stipple.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
