+++
showonlyimage = true
draft = false
image = "img/Paschal_Candle_Symbols_2015.svg"
date = "2019-01-01T21:12:22+05:30"
title = "Paschal-Candle-Symbols-2015"
weight = 0
+++

![Right-click to Save image](/img/Paschal_Candle_Symbols_2015.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
