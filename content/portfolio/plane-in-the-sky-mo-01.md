+++
showonlyimage = true
draft = false
image = "img/plane_in_the_sky_mo_01.svg"
date = "2019-01-01T21:12:22+05:30"
title = "plane-in-the-sky-mo-01"
weight = 0
+++

![Right-click to Save image](/img/plane_in_the_sky_mo_01.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
