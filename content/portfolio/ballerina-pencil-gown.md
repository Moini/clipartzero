+++
showonlyimage = true
draft = false
image = "img/ballerina_pencil_gown.svg"
date = "2019-01-01T21:12:22+05:30"
title = "ballerina-pencil-gown"
weight = 0
+++

![Right-click to Save image](/img/ballerina_pencil_gown.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
