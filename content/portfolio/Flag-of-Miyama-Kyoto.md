+++
showonlyimage = true
draft = false
image = "img/Flag_of_Miyama_Kyoto.svg"
date = "2019-01-01T21:12:22+05:30"
title = "Flag-of-Miyama-Kyoto"
weight = 0
+++

![Right-click to Save image](/img/Flag_of_Miyama_Kyoto.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
