+++
showonlyimage = true
draft = false
image = "img/Heartbleed-Patch-Needed-street-art.svg"
date = "2019-01-01T21:12:22+05:30"
title = "Heartbleed-Patch-Needed-street-art"
weight = 0
+++

![Right-click to Save image](/img/Heartbleed-Patch-Needed-street-art.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
