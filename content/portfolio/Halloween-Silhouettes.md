+++
showonlyimage = true
draft = false
image = "img/Halloween_Silhouettes.svg"
date = "2019-01-01T21:12:22+05:30"
title = "Halloween-Silhouettes"
weight = 0
+++

![Right-click to Save image](/img/Halloween_Silhouettes.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
