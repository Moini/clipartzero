+++
showonlyimage = true
draft = false
image = "img/Bookshelves-with-books.svg"
date = "2019-01-01T21:12:22+05:30"
title = "Bookshelves-with-books"
weight = 0
+++

![Right-click to Save image](/img/Bookshelves-with-books.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
