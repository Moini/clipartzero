+++
showonlyimage = true
draft = false
image = "img/gatepost_finial.svg"
date = "2019-01-01T21:12:22+05:30"
title = "gatepost-finial"
weight = 0
+++

![Right-click to Save image](/img/gatepost_finial.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
