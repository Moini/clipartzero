+++
showonlyimage = true
draft = false
image = "img/small_water_bottle.svg"
date = "2019-01-01T21:12:22+05:30"
title = "small-water-bottle"
weight = 0
+++

![Right-click to Save image](/img/small_water_bottle.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
