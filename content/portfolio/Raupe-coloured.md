+++
showonlyimage = true
draft = false
image = "img/Raupe-coloured.svg"
date = "2019-01-01T21:12:22+05:30"
title = "Raupe-coloured"
weight = 0
+++

![Right-click to Save image](/img/Raupe-coloured.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
