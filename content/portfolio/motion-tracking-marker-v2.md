+++
showonlyimage = true
draft = false
image = "img/motion_tracking_marker_v2.svg"
date = "2019-01-01T21:12:22+05:30"
title = "motion-tracking-marker-v2"
weight = 0
+++

![Right-click to Save image](/img/motion_tracking_marker_v2.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
