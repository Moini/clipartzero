+++
showonlyimage = true
draft = false
image = "img/head_of_tux_green.svg"
date = "2019-01-01T21:12:22+05:30"
title = "head-of-tux-green"
weight = 0
+++

![Right-click to Save image](/img/head_of_tux_green.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
