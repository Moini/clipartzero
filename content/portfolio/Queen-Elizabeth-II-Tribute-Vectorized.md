+++
showonlyimage = true
draft = false
image = "img/Queen-Elizabeth-II-Tribute-Vectorized.svg"
date = "2019-01-01T21:12:22+05:30"
title = "Queen-Elizabeth-II-Tribute-Vectorized"
weight = 0
+++

![Right-click to Save image](/img/Queen-Elizabeth-II-Tribute-Vectorized.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
