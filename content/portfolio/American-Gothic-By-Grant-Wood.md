+++
showonlyimage = true
draft = false
image = "img/American-Gothic-By-Grant-Wood.svg"
date = "2019-01-01T21:12:22+05:30"
title = "American-Gothic-By-Grant-Wood"
weight = 0
+++

![Right-click to Save image](/img/American-Gothic-By-Grant-Wood.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
