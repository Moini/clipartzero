+++
showonlyimage = true
draft = false
image = "img/Play-with-Openclipart-on-your-iPad-with-Inkpad.svg"
date = "2019-01-01T21:12:22+05:30"
title = "Play-with-Openclipart-on-your-iPad-with-Inkpad"
weight = 0
+++

![Right-click to Save image](/img/Play-with-Openclipart-on-your-iPad-with-Inkpad.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
