+++
showonlyimage = true
draft = false
image = "img/Stainless-Steel-Heart.svg"
date = "2019-01-01T21:12:22+05:30"
title = "Stainless-Steel-Heart"
weight = 0
+++

![Right-click to Save image](/img/Stainless-Steel-Heart.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
