+++
showonlyimage = true
draft = false
image = "img/publicdomainq-politician_oliver-cromwell.svg"
date = "2019-01-01T21:12:22+05:30"
title = "publicdomainq-politician-oliver-cromwell"
weight = 0
+++

![Right-click to Save image](/img/publicdomainq-politician_oliver-cromwell.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
