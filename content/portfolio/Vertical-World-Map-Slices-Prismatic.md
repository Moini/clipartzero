+++
showonlyimage = true
draft = false
image = "img/Vertical-World-Map-Slices-Prismatic.svg"
date = "2019-01-01T21:12:22+05:30"
title = "Vertical-World-Map-Slices-Prismatic"
weight = 0
+++

![Right-click to Save image](/img/Vertical-World-Map-Slices-Prismatic.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
