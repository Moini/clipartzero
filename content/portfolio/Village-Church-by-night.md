+++
showonlyimage = true
draft = false
image = "img/Village-Church_by_night.svg"
date = "2019-01-01T21:12:22+05:30"
title = "Village-Church-by-night"
weight = 0
+++

![Right-click to Save image](/img/Village-Church_by_night.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
