+++
showonlyimage = true
draft = false
image = "img/TJ-Openclipart-93-lighthouse-illustrated-21-2-17--final.svg"
date = "2019-01-01T21:12:22+05:30"
title = "TJ-Openclipart-93-lighthouse-illustrated-21-2-17--final"
weight = 0
+++

![Right-click to Save image](/img/TJ-Openclipart-93-lighthouse-illustrated-21-2-17--final.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
