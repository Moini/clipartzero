+++
showonlyimage = true
draft = false
image = "img/configure_shortcuts.svg"
date = "2019-01-01T21:12:22+05:30"
title = "configure-shortcuts"
weight = 0
+++

![Right-click to Save image](/img/configure_shortcuts.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
