+++
showonlyimage = true
draft = false
image = "img/info_symbol_lime.svg"
date = "2019-01-01T21:12:22+05:30"
title = "info-symbol-lime"
weight = 0
+++

![Right-click to Save image](/img/info_symbol_lime.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
