+++
showonlyimage = true
draft = false
image = "img/Greek-Orthodox-Jesus-Painting.svg"
date = "2019-01-01T21:12:22+05:30"
title = "Greek-Orthodox-Jesus-Painting"
weight = 0
+++

![Right-click to Save image](/img/Greek-Orthodox-Jesus-Painting.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
