+++
showonlyimage = true
draft = false
image = "img/Shutter-Silhouette-Mark-III-Prismatic-5-And-A-Half.svg"
date = "2019-01-01T21:12:22+05:30"
title = "Shutter-Silhouette-Mark-III-Prismatic-5-And-A-Half"
weight = 0
+++

![Right-click to Save image](/img/Shutter-Silhouette-Mark-III-Prismatic-5-And-A-Half.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
