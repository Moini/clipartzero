+++
showonlyimage = true
draft = false
image = "img/World_in_Hand_Big.svg"
date = "2019-01-01T21:12:22+05:30"
title = "World-in-Hand-Big"
weight = 0
+++

![Right-click to Save image](/img/World_in_Hand_Big.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
