+++
showonlyimage = true
draft = false
image = "img/Low-Poly-Prismatic-Stylized-Birds-Silhouette.svg"
date = "2019-01-01T21:12:22+05:30"
title = "Low-Poly-Prismatic-Stylized-Birds-Silhouette"
weight = 0
+++

![Right-click to Save image](/img/Low-Poly-Prismatic-Stylized-Birds-Silhouette.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
