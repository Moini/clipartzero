+++
showonlyimage = true
draft = false
image = "img/London-Tower-Bridge.svg"
date = "2019-01-01T21:12:22+05:30"
title = "London-Tower-Bridge"
weight = 0
+++

![Right-click to Save image](/img/London-Tower-Bridge.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
