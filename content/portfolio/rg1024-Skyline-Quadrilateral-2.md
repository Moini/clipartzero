+++
showonlyimage = true
draft = false
image = "img/rg1024-Skyline-Quadrilateral-2.svg"
date = "2019-01-01T21:12:22+05:30"
title = "rg1024-Skyline-Quadrilateral-2"
weight = 0
+++

![Right-click to Save image](/img/rg1024-Skyline-Quadrilateral-2.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
