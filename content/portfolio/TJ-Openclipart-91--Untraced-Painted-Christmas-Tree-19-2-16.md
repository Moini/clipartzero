+++
showonlyimage = true
draft = false
image = "img/TJ-Openclipart-91--Untraced-Painted-Christmas-Tree-19-2-16.svg"
date = "2019-01-01T21:12:22+05:30"
title = "TJ-Openclipart-91--Untraced-Painted-Christmas-Tree-19-2-16"
weight = 0
+++

![Right-click to Save image](/img/TJ-Openclipart-91--Untraced-Painted-Christmas-Tree-19-2-16.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
