+++
showonlyimage = true
draft = false
image = "img/sun-behind-clouds.svg"
date = "2019-01-01T21:12:22+05:30"
title = "sun-behind-clouds"
weight = 0
+++

![Right-click to Save image](/img/sun-behind-clouds.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
