+++
showonlyimage = true
draft = false
image = "img/cc-by-nc-saAr-printable.svg"
date = "2019-01-01T21:12:22+05:30"
title = "cc-by-nc-saAr-printable"
weight = 0
+++

![Right-click to Save image](/img/cc-by-nc-saAr-printable.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
