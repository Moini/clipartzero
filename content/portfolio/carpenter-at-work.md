+++
showonlyimage = true
draft = false
image = "img/carpenter_at_work.svg"
date = "2019-01-01T21:12:22+05:30"
title = "carpenter-at-work"
weight = 0
+++

![Right-click to Save image](/img/carpenter_at_work.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
