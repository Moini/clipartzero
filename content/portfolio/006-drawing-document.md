+++
showonlyimage = true
draft = false
image = "img/006_drawing_document.svg"
date = "2019-01-01T21:12:22+05:30"
title = "006-drawing-document"
weight = 0
+++

![Right-click to Save image](/img/006_drawing_document.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
