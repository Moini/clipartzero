+++
showonlyimage = true
draft = false
image = "img/EricOrtner_Gingerbread_House.svg"
date = "2019-01-01T21:12:22+05:30"
title = "EricOrtner-Gingerbread-House"
weight = 0
+++

![Right-click to Save image](/img/EricOrtner_Gingerbread_House.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
