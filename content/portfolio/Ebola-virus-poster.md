+++
showonlyimage = true
draft = false
image = "img/Ebola_virus_poster.svg"
date = "2019-01-01T21:12:22+05:30"
title = "Ebola-virus-poster"
weight = 0
+++

![Right-click to Save image](/img/Ebola_virus_poster.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
