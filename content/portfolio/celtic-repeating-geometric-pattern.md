+++
showonlyimage = true
draft = false
image = "img/celtic_repeating_geometric_pattern.svg"
date = "2019-01-01T21:12:22+05:30"
title = "celtic-repeating-geometric-pattern"
weight = 0
+++

![Right-click to Save image](/img/celtic_repeating_geometric_pattern.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
