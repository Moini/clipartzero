+++
showonlyimage = true
draft = false
image = "img/SRD-strawberry-smoothie.svg"
date = "2019-01-01T21:12:22+05:30"
title = "SRD-strawberry-smoothie"
weight = 0
+++

![Right-click to Save image](/img/SRD-strawberry-smoothie.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
