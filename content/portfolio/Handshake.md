+++
showonlyimage = true
draft = false
image = "img/Handshake.svg"
date = "2019-01-01T21:12:22+05:30"
title = "Handshake"
weight = 0
+++

![Right-click to Save image](/img/Handshake.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
