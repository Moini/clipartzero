+++
showonlyimage = true
draft = false
image = "img/Low-Poly-3D-Heart-Red.svg"
date = "2019-01-01T21:12:22+05:30"
title = "Low-Poly-3D-Heart-Red"
weight = 0
+++

![Right-click to Save image](/img/Low-Poly-3D-Heart-Red.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
