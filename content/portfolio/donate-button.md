+++
showonlyimage = true
draft = false
image = "img/donate-button.svg"
date = "2019-01-01T21:12:22+05:30"
title = "donate-button"
weight = 0
+++

![Right-click to Save image](/img/donate-button.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
