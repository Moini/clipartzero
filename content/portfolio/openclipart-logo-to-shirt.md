+++
showonlyimage = true
draft = false
image = "img/openclipart_logo_to_shirt.svg"
date = "2019-01-01T21:12:22+05:30"
title = "openclipart-logo-to-shirt"
weight = 0
+++

![Right-click to Save image](/img/openclipart_logo_to_shirt.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
