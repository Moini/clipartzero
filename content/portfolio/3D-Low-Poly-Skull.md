+++
showonlyimage = true
draft = false
image = "img/3D-Low-Poly-Skull.svg"
date = "2019-01-01T21:12:22+05:30"
title = "3D-Low-Poly-Skull"
weight = 0
+++

![Right-click to Save image](/img/3D-Low-Poly-Skull.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
