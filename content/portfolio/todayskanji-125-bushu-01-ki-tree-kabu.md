+++
showonlyimage = true
draft = false
image = "img/todayskanji_125_bushu_01_ki_tree_kabu.svg"
date = "2019-01-01T21:12:22+05:30"
title = "todayskanji-125-bushu-01-ki-tree-kabu"
weight = 0
+++

![Right-click to Save image](/img/todayskanji_125_bushu_01_ki_tree_kabu.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
