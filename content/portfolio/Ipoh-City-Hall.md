+++
showonlyimage = true
draft = false
image = "img/Ipoh-City-Hall.svg"
date = "2019-01-01T21:12:22+05:30"
title = "Ipoh-City-Hall"
weight = 0
+++

![Right-click to Save image](/img/Ipoh-City-Hall.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
