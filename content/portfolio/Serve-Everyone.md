+++
showonlyimage = true
draft = false
image = "img/Serve_Everyone.svg"
date = "2019-01-01T21:12:22+05:30"
title = "Serve-Everyone"
weight = 0
+++

![Right-click to Save image](/img/Serve_Everyone.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
