+++
showonlyimage = true
draft = false
image = "img/desert_island_black_and_white.svg"
date = "2019-01-01T21:12:22+05:30"
title = "desert-island-black-and-white"
weight = 0
+++

![Right-click to Save image](/img/desert_island_black_and_white.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
