+++
showonlyimage = true
draft = false
image = "img/flower_petal_combo_5_petal_heart_template_outline.svg"
date = "2019-01-01T21:12:22+05:30"
title = "flower-petal-combo-5-petal-heart-template-outline"
weight = 0
+++

![Right-click to Save image](/img/flower_petal_combo_5_petal_heart_template_outline.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
