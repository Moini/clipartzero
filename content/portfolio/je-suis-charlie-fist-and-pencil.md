+++
showonlyimage = true
draft = false
image = "img/je_suis_charlie_fist_and_pencil.svg"
date = "2019-01-01T21:12:22+05:30"
title = "je-suis-charlie-fist-and-pencil"
weight = 0
+++

![Right-click to Save image](/img/je_suis_charlie_fist_and_pencil.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
