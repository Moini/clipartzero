+++
showonlyimage = true
draft = false
image = "img/Flower_Petal_1_combo_8_petals_poinsettia.svg"
date = "2019-01-01T21:12:22+05:30"
title = "Flower-Petal-1-combo-8-petals-poinsettia"
weight = 0
+++

![Right-click to Save image](/img/Flower_Petal_1_combo_8_petals_poinsettia.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
