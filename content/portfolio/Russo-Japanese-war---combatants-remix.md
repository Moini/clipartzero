+++
showonlyimage = true
draft = false
image = "img/Russo-Japanese_war_-_combatants_remix.svg"
date = "2019-01-01T21:12:22+05:30"
title = "Russo-Japanese-war---combatants-remix"
weight = 0
+++

![Right-click to Save image](/img/Russo-Japanese_war_-_combatants_remix.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
