+++
showonlyimage = true
draft = false
image = "img/alexander-graham-bell.svg"
date = "2019-01-01T21:12:22+05:30"
title = "alexander-graham-bell"
weight = 0
+++

![Right-click to Save image](/img/alexander-graham-bell.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
