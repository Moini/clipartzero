+++
showonlyimage = true
draft = false
image = "img/Gold-Floral-Crescent-Moon-Mark-II-2.svg"
date = "2019-01-01T21:12:22+05:30"
title = "Gold-Floral-Crescent-Moon-Mark-II-2"
weight = 0
+++

![Right-click to Save image](/img/Gold-Floral-Crescent-Moon-Mark-II-2.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
