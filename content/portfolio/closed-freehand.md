+++
showonlyimage = true
draft = false
image = "img/closed_freehand.svg"
date = "2019-01-01T21:12:22+05:30"
title = "closed-freehand"
weight = 0
+++

![Right-click to Save image](/img/closed_freehand.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
