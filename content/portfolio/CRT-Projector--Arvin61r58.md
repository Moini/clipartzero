+++
showonlyimage = true
draft = false
image = "img/CRT_Projector__Arvin61r58.svg"
date = "2019-01-01T21:12:22+05:30"
title = "CRT-Projector--Arvin61r58"
weight = 0
+++

![Right-click to Save image](/img/CRT_Projector__Arvin61r58.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
