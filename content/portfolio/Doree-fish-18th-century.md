+++
showonlyimage = true
draft = false
image = "img/Doree_fish_18th_century.svg"
date = "2019-01-01T21:12:22+05:30"
title = "Doree-fish-18th-century"
weight = 0
+++

![Right-click to Save image](/img/Doree_fish_18th_century.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
