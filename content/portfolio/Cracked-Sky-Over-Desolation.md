+++
showonlyimage = true
draft = false
image = "img/Cracked-Sky-Over-Desolation.svg"
date = "2019-01-01T21:12:22+05:30"
title = "Cracked-Sky-Over-Desolation"
weight = 0
+++

![Right-click to Save image](/img/Cracked-Sky-Over-Desolation.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
