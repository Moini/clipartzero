+++
showonlyimage = true
draft = false
image = "img/Flower_petal_2_ten_layers_stacked.svg"
date = "2019-01-01T21:12:22+05:30"
title = "Flower-petal-2-ten-layers-stacked"
weight = 0
+++

![Right-click to Save image](/img/Flower_petal_2_ten_layers_stacked.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
