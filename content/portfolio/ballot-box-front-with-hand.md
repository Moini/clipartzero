+++
showonlyimage = true
draft = false
image = "img/ballot_box_front_with_hand.svg"
date = "2019-01-01T21:12:22+05:30"
title = "ballot-box-front-with-hand"
weight = 0
+++

![Right-click to Save image](/img/ballot_box_front_with_hand.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
