+++
showonlyimage = true
draft = false
image = "img/Have-you-tried-Openclipart-within-Google-Docs.svg"
date = "2019-01-01T21:12:22+05:30"
title = "Have-you-tried-Openclipart-within-Google-Docs"
weight = 0
+++

![Right-click to Save image](/img/Have-you-tried-Openclipart-within-Google-Docs.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
