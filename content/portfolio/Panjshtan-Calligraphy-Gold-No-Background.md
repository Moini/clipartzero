+++
showonlyimage = true
draft = false
image = "img/Panjshtan-Calligraphy-Gold-No-Background.svg"
date = "2019-01-01T21:12:22+05:30"
title = "Panjshtan-Calligraphy-Gold-No-Background"
weight = 0
+++

![Right-click to Save image](/img/Panjshtan-Calligraphy-Gold-No-Background.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
