+++
showonlyimage = true
draft = false
image = "img/Eric-Shinseki-official-Veterans-Affairs-Portrait-Colorized.svg"
date = "2019-01-01T21:12:22+05:30"
title = "Eric-Shinseki-official-Veterans-Affairs-Portrait-Colorized"
weight = 0
+++

![Right-click to Save image](/img/Eric-Shinseki-official-Veterans-Affairs-Portrait-Colorized.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
