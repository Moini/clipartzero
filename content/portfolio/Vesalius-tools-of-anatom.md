+++
showonlyimage = true
draft = false
image = "img/Vesalius_tools_of_anatom.svg"
date = "2019-01-01T21:12:22+05:30"
title = "Vesalius-tools-of-anatom"
weight = 0
+++

![Right-click to Save image](/img/Vesalius_tools_of_anatom.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
