+++
showonlyimage = true
draft = false
image = "img/PSM_V07_D144_Common_meadow_mushroom_colored.svg"
date = "2019-01-01T21:12:22+05:30"
title = "PSM-V07-D144-Common-meadow-mushroom-colored"
weight = 0
+++

![Right-click to Save image](/img/PSM_V07_D144_Common_meadow_mushroom_colored.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
