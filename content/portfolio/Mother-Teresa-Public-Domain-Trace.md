+++
showonlyimage = true
draft = false
image = "img/Mother-Teresa-Public-Domain-Trace.svg"
date = "2019-01-01T21:12:22+05:30"
title = "Mother-Teresa-Public-Domain-Trace"
weight = 0
+++

![Right-click to Save image](/img/Mother-Teresa-Public-Domain-Trace.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
