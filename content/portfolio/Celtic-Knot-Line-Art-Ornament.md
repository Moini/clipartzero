+++
showonlyimage = true
draft = false
image = "img/Celtic-Knot-Line-Art-Ornament.svg"
date = "2019-01-01T21:12:22+05:30"
title = "Celtic-Knot-Line-Art-Ornament"
weight = 0
+++

![Right-click to Save image](/img/Celtic-Knot-Line-Art-Ornament.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
