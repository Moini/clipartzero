+++
showonlyimage = true
draft = false
image = "img/99-Percent-Quality.svg"
date = "2019-01-01T21:12:22+05:30"
title = "99-Percent-Quality"
weight = 0
+++

![Right-click to Save image](/img/99-Percent-Quality.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
