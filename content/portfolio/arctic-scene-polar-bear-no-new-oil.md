+++
showonlyimage = true
draft = false
image = "img/arctic_scene_polar_bear_no_new_oil.svg"
date = "2019-01-01T21:12:22+05:30"
title = "arctic-scene-polar-bear-no-new-oil"
weight = 0
+++

![Right-click to Save image](/img/arctic_scene_polar_bear_no_new_oil.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
