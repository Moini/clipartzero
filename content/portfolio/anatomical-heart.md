+++
showonlyimage = true
draft = false
image = "img/anatomical_heart.svg"
date = "2019-01-01T21:12:22+05:30"
title = "anatomical-heart"
weight = 0
+++

![Right-click to Save image](/img/anatomical_heart.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
