+++
showonlyimage = true
draft = false
image = "img/camellia-with-bird.svg"
date = "2019-01-01T21:12:22+05:30"
title = "camellia-with-bird"
weight = 0
+++

![Right-click to Save image](/img/camellia-with-bird.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
