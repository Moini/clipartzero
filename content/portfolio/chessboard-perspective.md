+++
showonlyimage = true
draft = false
image = "img/chessboard-perspective.svg"
date = "2019-01-01T21:12:22+05:30"
title = "chessboard-perspective"
weight = 0
+++

![Right-click to Save image](/img/chessboard-perspective.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
