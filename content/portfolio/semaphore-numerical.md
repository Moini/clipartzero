+++
showonlyimage = true
draft = false
image = "img/semaphore_numerical.svg"
date = "2019-01-01T21:12:22+05:30"
title = "semaphore-numerical"
weight = 0
+++

![Right-click to Save image](/img/semaphore_numerical.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
