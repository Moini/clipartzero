+++
showonlyimage = true
draft = false
image = "img/Gold-Heart-Starburst.svg"
date = "2019-01-01T21:12:22+05:30"
title = "Gold-Heart-Starburst"
weight = 0
+++

![Right-click to Save image](/img/Gold-Heart-Starburst.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
