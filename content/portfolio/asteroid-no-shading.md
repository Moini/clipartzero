+++
showonlyimage = true
draft = false
image = "img/asteroid-no-shading.svg"
date = "2019-01-01T21:12:22+05:30"
title = "asteroid-no-shading"
weight = 0
+++

![Right-click to Save image](/img/asteroid-no-shading.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
