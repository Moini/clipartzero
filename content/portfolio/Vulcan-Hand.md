+++
showonlyimage = true
draft = false
image = "img/Vulcan_Hand.svg"
date = "2019-01-01T21:12:22+05:30"
title = "Vulcan-Hand"
weight = 0
+++

![Right-click to Save image](/img/Vulcan_Hand.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
