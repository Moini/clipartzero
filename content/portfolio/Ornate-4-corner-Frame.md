+++
showonlyimage = true
draft = false
image = "img/Ornate_4-corner_Frame.svg"
date = "2019-01-01T21:12:22+05:30"
title = "Ornate-4-corner-Frame"
weight = 0
+++

![Right-click to Save image](/img/Ornate_4-corner_Frame.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
