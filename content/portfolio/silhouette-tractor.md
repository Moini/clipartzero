+++
showonlyimage = true
draft = false
image = "img/silhouette_tractor.svg"
date = "2019-01-01T21:12:22+05:30"
title = "silhouette-tractor"
weight = 0
+++

![Right-click to Save image](/img/silhouette_tractor.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
