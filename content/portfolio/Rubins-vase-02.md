+++
showonlyimage = true
draft = false
image = "img/Rubins-vase-02.svg"
date = "2019-01-01T21:12:22+05:30"
title = "Rubins-vase-02"
weight = 0
+++

![Right-click to Save image](/img/Rubins-vase-02.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
