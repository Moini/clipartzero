+++
showonlyimage = true
draft = false
image = "img/Sunset_and_boat.svg"
date = "2019-01-01T21:12:22+05:30"
title = "Sunset-and-boat"
weight = 0
+++

![Right-click to Save image](/img/Sunset_and_boat.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
