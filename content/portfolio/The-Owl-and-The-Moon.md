+++
showonlyimage = true
draft = false
image = "img/The_Owl_and_The_Moon.svg"
date = "2019-01-01T21:12:22+05:30"
title = "The-Owl-and-The-Moon"
weight = 0
+++

![Right-click to Save image](/img/The_Owl_and_The_Moon.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
