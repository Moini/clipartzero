+++
showonlyimage = true
draft = false
image = "img/YBSLE-seamless-pattern-circle-and-square.svg"
date = "2019-01-01T21:12:22+05:30"
title = "YBSLE-seamless-pattern-circle-and-square"
weight = 0
+++

![Right-click to Save image](/img/YBSLE-seamless-pattern-circle-and-square.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
