+++
showonlyimage = true
draft = false
image = "img/Autumn-Trees-with-a-Bird.svg"
date = "2019-01-01T21:12:22+05:30"
title = "Autumn-Trees-with-a-Bird"
weight = 0
+++

![Right-click to Save image](/img/Autumn-Trees-with-a-Bird.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
