+++
showonlyimage = true
draft = false
image = "img/black_and_white_bishop_r.svg"
date = "2019-01-01T21:12:22+05:30"
title = "black-and-white-bishop-r"
weight = 0
+++

![Right-click to Save image](/img/black_and_white_bishop_r.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
