+++
showonlyimage = true
draft = false
image = "img/Hillary-Clinton-as-President-of-the-United-States-Sketch.svg"
date = "2019-01-01T21:12:22+05:30"
title = "Hillary-Clinton-as-President-of-the-United-States-Sketch"
weight = 0
+++

![Right-click to Save image](/img/Hillary-Clinton-as-President-of-the-United-States-Sketch.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
