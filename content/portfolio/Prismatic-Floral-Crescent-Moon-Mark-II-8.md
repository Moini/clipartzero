+++
showonlyimage = true
draft = false
image = "img/Prismatic-Floral-Crescent-Moon-Mark-II-8.svg"
date = "2019-01-01T21:12:22+05:30"
title = "Prismatic-Floral-Crescent-Moon-Mark-II-8"
weight = 0
+++

![Right-click to Save image](/img/Prismatic-Floral-Crescent-Moon-Mark-II-8.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
