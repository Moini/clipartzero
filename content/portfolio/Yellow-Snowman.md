+++
showonlyimage = true
draft = false
image = "img/Yellow-Snowman.svg"
date = "2019-01-01T21:12:22+05:30"
title = "Yellow-Snowman"
weight = 0
+++

![Right-click to Save image](/img/Yellow-Snowman.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
