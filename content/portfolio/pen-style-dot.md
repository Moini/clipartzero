+++
showonlyimage = true
draft = false
image = "img/pen_style_dot.svg"
date = "2019-01-01T21:12:22+05:30"
title = "pen-style-dot"
weight = 0
+++

![Right-click to Save image](/img/pen_style_dot.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
