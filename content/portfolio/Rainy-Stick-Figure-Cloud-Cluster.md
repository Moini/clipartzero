+++
showonlyimage = true
draft = false
image = "img/Rainy_Stick_Figure_Cloud_Cluster.svg"
date = "2019-01-01T21:12:22+05:30"
title = "Rainy-Stick-Figure-Cloud-Cluster"
weight = 0
+++

![Right-click to Save image](/img/Rainy_Stick_Figure_Cloud_Cluster.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
