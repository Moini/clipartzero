+++
showonlyimage = true
draft = false
image = "img/Portcullis_Wood_Iron_Cardstock.svg"
date = "2019-01-01T21:12:22+05:30"
title = "Portcullis-Wood-Iron-Cardstock"
weight = 0
+++

![Right-click to Save image](/img/Portcullis_Wood_Iron_Cardstock.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
