+++
showonlyimage = true
draft = false
image = "img/Prismatic-Abstract-Worm.svg"
date = "2019-01-01T21:12:22+05:30"
title = "Prismatic-Abstract-Worm"
weight = 0
+++

![Right-click to Save image](/img/Prismatic-Abstract-Worm.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
