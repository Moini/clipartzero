+++
showonlyimage = true
draft = false
image = "img/Pegacorn_purple_silver_Woofer.svg"
date = "2019-01-01T21:12:22+05:30"
title = "Pegacorn-purple-silver-Woofer"
weight = 0
+++

![Right-click to Save image](/img/Pegacorn_purple_silver_Woofer.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
