+++
showonlyimage = true
draft = false
image = "img/Blue-Eyed-Cat.svg"
date = "2019-01-01T21:12:22+05:30"
title = "Blue-Eyed-Cat"
weight = 0
+++

![Right-click to Save image](/img/Blue-Eyed-Cat.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
