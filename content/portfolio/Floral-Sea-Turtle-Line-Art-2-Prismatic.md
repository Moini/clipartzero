+++
showonlyimage = true
draft = false
image = "img/Floral-Sea-Turtle-Line-Art-2-Prismatic.svg"
date = "2019-01-01T21:12:22+05:30"
title = "Floral-Sea-Turtle-Line-Art-2-Prismatic"
weight = 0
+++

![Right-click to Save image](/img/Floral-Sea-Turtle-Line-Art-2-Prismatic.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
