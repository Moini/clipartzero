+++
showonlyimage = true
draft = false
image = "img/mountains_with_waterfall.svg"
date = "2019-01-01T21:12:22+05:30"
title = "mountains-with-waterfall"
weight = 0
+++

![Right-click to Save image](/img/mountains_with_waterfall.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
