+++
showonlyimage = true
draft = false
image = "img/Mr_Penguin.svg"
date = "2019-01-01T21:12:22+05:30"
title = "Mr-Penguin"
weight = 0
+++

![Right-click to Save image](/img/Mr_Penguin.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
