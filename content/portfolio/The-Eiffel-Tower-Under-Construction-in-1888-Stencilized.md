+++
showonlyimage = true
draft = false
image = "img/The-Eiffel-Tower-Under-Construction-in-1888-Stencilized.svg"
date = "2019-01-01T21:12:22+05:30"
title = "The-Eiffel-Tower-Under-Construction-in-1888-Stencilized"
weight = 0
+++

![Right-click to Save image](/img/The-Eiffel-Tower-Under-Construction-in-1888-Stencilized.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
