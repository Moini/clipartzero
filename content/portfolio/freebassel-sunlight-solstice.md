+++
showonlyimage = true
draft = false
image = "img/freebassel_sunlight_solstice.svg"
date = "2019-01-01T21:12:22+05:30"
title = "freebassel-sunlight-solstice"
weight = 0
+++

![Right-click to Save image](/img/freebassel_sunlight_solstice.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
