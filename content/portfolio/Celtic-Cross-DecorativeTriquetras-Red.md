+++
showonlyimage = true
draft = false
image = "img/Celtic-Cross-DecorativeTriquetras-Red.svg"
date = "2019-01-01T21:12:22+05:30"
title = "Celtic-Cross-DecorativeTriquetras-Red"
weight = 0
+++

![Right-click to Save image](/img/Celtic-Cross-DecorativeTriquetras-Red.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
