+++
showonlyimage = true
draft = false
image = "img/QMC.media-BW-logo.svg"
date = "2019-01-01T21:12:22+05:30"
title = "QMC.media-BW-logo"
weight = 0
+++

![Right-click to Save image](/img/QMC.media-BW-logo.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
