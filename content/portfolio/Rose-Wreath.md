+++
showonlyimage = true
draft = false
image = "img/Rose-Wreath.svg"
date = "2019-01-01T21:12:22+05:30"
title = "Rose-Wreath"
weight = 0
+++

![Right-click to Save image](/img/Rose-Wreath.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
