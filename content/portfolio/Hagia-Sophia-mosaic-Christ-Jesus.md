+++
showonlyimage = true
draft = false
image = "img/Hagia-Sophia-mosaic-Christ-Jesus.svg"
date = "2019-01-01T21:12:22+05:30"
title = "Hagia-Sophia-mosaic-Christ-Jesus"
weight = 0
+++

![Right-click to Save image](/img/Hagia-Sophia-mosaic-Christ-Jesus.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
