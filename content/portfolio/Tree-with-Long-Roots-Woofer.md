+++
showonlyimage = true
draft = false
image = "img/Tree_with_Long_Roots_Woofer.svg"
date = "2019-01-01T21:12:22+05:30"
title = "Tree-with-Long-Roots-Woofer"
weight = 0
+++

![Right-click to Save image](/img/Tree_with_Long_Roots_Woofer.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
