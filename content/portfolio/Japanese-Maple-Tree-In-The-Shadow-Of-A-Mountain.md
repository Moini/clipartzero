+++
showonlyimage = true
draft = false
image = "img/Japanese-Maple-Tree-In-The-Shadow-Of-A-Mountain.svg"
date = "2019-01-01T21:12:22+05:30"
title = "Japanese-Maple-Tree-In-The-Shadow-Of-A-Mountain"
weight = 0
+++

![Right-click to Save image](/img/Japanese-Maple-Tree-In-The-Shadow-Of-A-Mountain.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
