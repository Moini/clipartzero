+++
showonlyimage = true
draft = false
image = "img/Complex-Prismatic-Tree-Variation-2-No-Background.svg"
date = "2019-01-01T21:12:22+05:30"
title = "Complex-Prismatic-Tree-Variation-2-No-Background"
weight = 0
+++

![Right-click to Save image](/img/Complex-Prismatic-Tree-Variation-2-No-Background.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
