+++
showonlyimage = true
draft = false
image = "img/DailySketch-35-Eliot-Ness-and-The-Untouchables-2015070642.svg"
date = "2019-01-01T21:12:22+05:30"
title = "DailySketch-35-Eliot-Ness-and-The-Untouchables-2015070642"
weight = 0
+++

![Right-click to Save image](/img/DailySketch-35-Eliot-Ness-and-The-Untouchables-2015070642.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
