+++
showonlyimage = true
draft = false
image = "img/openclipart-logo-grey.svg"
date = "2019-01-01T21:12:22+05:30"
title = "openclipart-logo-grey"
weight = 0
+++

![Right-click to Save image](/img/openclipart-logo-grey.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
