+++
showonlyimage = true
draft = false
image = "img/rodentia-icons_document-recent.svg"
date = "2019-01-01T21:12:22+05:30"
title = "rodentia-icons-document-recent"
weight = 0
+++

![Right-click to Save image](/img/rodentia-icons_document-recent.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
