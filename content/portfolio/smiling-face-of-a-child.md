+++
showonlyimage = true
draft = false
image = "img/smiling_face_of_a_child.svg"
date = "2019-01-01T21:12:22+05:30"
title = "smiling-face-of-a-child"
weight = 0
+++

![Right-click to Save image](/img/smiling_face_of_a_child.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
