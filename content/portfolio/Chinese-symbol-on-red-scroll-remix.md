+++
showonlyimage = true
draft = false
image = "img/Chinese_symbol_on_red_scroll_remix.svg"
date = "2019-01-01T21:12:22+05:30"
title = "Chinese-symbol-on-red-scroll-remix"
weight = 0
+++

![Right-click to Save image](/img/Chinese_symbol_on_red_scroll_remix.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
