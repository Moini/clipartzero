+++
showonlyimage = true
draft = false
image = "img/Celtic-Knot-Line-Art-Butterfly.svg"
date = "2019-01-01T21:12:22+05:30"
title = "Celtic-Knot-Line-Art-Butterfly"
weight = 0
+++

![Right-click to Save image](/img/Celtic-Knot-Line-Art-Butterfly.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
