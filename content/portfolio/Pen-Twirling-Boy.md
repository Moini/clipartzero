+++
showonlyimage = true
draft = false
image = "img/Pen-Twirling-Boy.svg"
date = "2019-01-01T21:12:22+05:30"
title = "Pen-Twirling-Boy"
weight = 0
+++

![Right-click to Save image](/img/Pen-Twirling-Boy.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
