+++
showonlyimage = true
draft = false
image = "img/The-Old-Mill-Stream.svg"
date = "2019-01-01T21:12:22+05:30"
title = "The-Old-Mill-Stream"
weight = 0
+++

![Right-click to Save image](/img/The-Old-Mill-Stream.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
