+++
showonlyimage = true
draft = false
image = "img/IO-stack-of-the-Linux-kernel-diag.svg"
date = "2019-01-01T21:12:22+05:30"
title = "IO-stack-of-the-Linux-kernel-diag"
weight = 0
+++

![Right-click to Save image](/img/IO-stack-of-the-Linux-kernel-diag.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
