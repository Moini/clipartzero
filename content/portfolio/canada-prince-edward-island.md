+++
showonlyimage = true
draft = false
image = "img/canada_prince_edward_island.svg"
date = "2019-01-01T21:12:22+05:30"
title = "canada-prince-edward-island"
weight = 0
+++

![Right-click to Save image](/img/canada_prince_edward_island.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
