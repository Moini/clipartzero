+++
showonlyimage = true
draft = false
image = "img/addressed_envelope_with_stamp_01.svg"
date = "2019-01-01T21:12:22+05:30"
title = "addressed-envelope-with-stamp-01"
weight = 0
+++

![Right-click to Save image](/img/addressed_envelope_with_stamp_01.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
