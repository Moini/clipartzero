+++
showonlyimage = true
draft = false
image = "img/trust_territory_of_the_pacific_islands.svg"
date = "2019-01-01T21:12:22+05:30"
title = "trust-territory-of-the-pacific-islands"
weight = 0
+++

![Right-click to Save image](/img/trust_territory_of_the_pacific_islands.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
