+++
showonlyimage = true
draft = false
image = "img/AnimalSilhouettes2-Fox.svg"
date = "2019-01-01T21:12:22+05:30"
title = "AnimalSilhouettes2-Fox"
weight = 0
+++

![Right-click to Save image](/img/AnimalSilhouettes2-Fox.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
