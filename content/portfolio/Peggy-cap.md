+++
showonlyimage = true
draft = false
image = "img/Peggy_cap.svg"
date = "2019-01-01T21:12:22+05:30"
title = "Peggy-cap"
weight = 0
+++

![Right-click to Save image](/img/Peggy_cap.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
