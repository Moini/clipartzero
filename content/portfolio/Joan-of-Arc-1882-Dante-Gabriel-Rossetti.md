+++
showonlyimage = true
draft = false
image = "img/Joan-of-Arc-1882-Dante-Gabriel-Rossetti.svg"
date = "2019-01-01T21:12:22+05:30"
title = "Joan-of-Arc-1882-Dante-Gabriel-Rossetti"
weight = 0
+++

![Right-click to Save image](/img/Joan-of-Arc-1882-Dante-Gabriel-Rossetti.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
