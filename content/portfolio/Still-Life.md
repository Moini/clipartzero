+++
showonlyimage = true
draft = false
image = "img/Still-Life.svg"
date = "2019-01-01T21:12:22+05:30"
title = "Still-Life"
weight = 0
+++

![Right-click to Save image](/img/Still-Life.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
