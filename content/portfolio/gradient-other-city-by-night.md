+++
showonlyimage = true
draft = false
image = "img/gradient-other-city-by-night.svg"
date = "2019-01-01T21:12:22+05:30"
title = "gradient-other-city-by-night"
weight = 0
+++

![Right-click to Save image](/img/gradient-other-city-by-night.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
