+++
showonlyimage = true
draft = false
image = "img/Pizza_Pepperoni.svg"
date = "2019-01-01T21:12:22+05:30"
title = "Pizza-Pepperoni"
weight = 0
+++

![Right-click to Save image](/img/Pizza_Pepperoni.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
