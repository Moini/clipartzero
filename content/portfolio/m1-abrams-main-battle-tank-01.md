+++
showonlyimage = true
draft = false
image = "img/m1_abrams_main_battle_tank_01.svg"
date = "2019-01-01T21:12:22+05:30"
title = "m1-abrams-main-battle-tank-01"
weight = 0
+++

![Right-click to Save image](/img/m1_abrams_main_battle_tank_01.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
