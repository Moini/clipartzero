+++
showonlyimage = true
draft = false
image = "img/Asian-dragon-vignette.svg"
date = "2019-01-01T21:12:22+05:30"
title = "Asian-dragon-vignette"
weight = 0
+++

![Right-click to Save image](/img/Asian-dragon-vignette.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
