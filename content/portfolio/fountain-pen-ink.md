+++
showonlyimage = true
draft = false
image = "img/fountain_pen_ink.svg"
date = "2019-01-01T21:12:22+05:30"
title = "fountain-pen-ink"
weight = 0
+++

![Right-click to Save image](/img/fountain_pen_ink.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
