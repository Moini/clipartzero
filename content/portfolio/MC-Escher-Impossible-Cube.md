+++
showonlyimage = true
draft = false
image = "img/MC_Escher_Impossible_Cube.svg"
date = "2019-01-01T21:12:22+05:30"
title = "MC-Escher-Impossible-Cube"
weight = 0
+++

![Right-click to Save image](/img/MC_Escher_Impossible_Cube.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
