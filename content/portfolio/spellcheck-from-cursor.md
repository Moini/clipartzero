+++
showonlyimage = true
draft = false
image = "img/spellcheck_from_cursor.svg"
date = "2019-01-01T21:12:22+05:30"
title = "spellcheck-from-cursor"
weight = 0
+++

![Right-click to Save image](/img/spellcheck_from_cursor.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
