+++
showonlyimage = true
draft = false
image = "img/Horizontal-Situation-Indicator-with-flags.svg"
date = "2019-01-01T21:12:22+05:30"
title = "Horizontal-Situation-Indicator-with-flags"
weight = 0
+++

![Right-click to Save image](/img/Horizontal-Situation-Indicator-with-flags.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
