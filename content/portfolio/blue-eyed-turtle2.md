+++
showonlyimage = true
draft = false
image = "img/blue_eyed_turtle2.svg"
date = "2019-01-01T21:12:22+05:30"
title = "blue-eyed-turtle2"
weight = 0
+++

![Right-click to Save image](/img/blue_eyed_turtle2.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
