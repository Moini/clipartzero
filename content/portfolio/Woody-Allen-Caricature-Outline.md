+++
showonlyimage = true
draft = false
image = "img/Woody-Allen-Caricature-Outline.svg"
date = "2019-01-01T21:12:22+05:30"
title = "Woody-Allen-Caricature-Outline"
weight = 0
+++

![Right-click to Save image](/img/Woody-Allen-Caricature-Outline.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
