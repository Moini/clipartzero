+++
showonlyimage = true
draft = false
image = "img/coat_of_arms_of_anglica_01.svg"
date = "2019-01-01T21:12:22+05:30"
title = "coat-of-arms-of-anglica-01"
weight = 0
+++

![Right-click to Save image](/img/coat_of_arms_of_anglica_01.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
