+++
showonlyimage = true
draft = false
image = "img/mouse_pointer_wolfram_es_02.svg"
date = "2019-01-01T21:12:22+05:30"
title = "mouse-pointer-wolfram-es-02"
weight = 0
+++

![Right-click to Save image](/img/mouse_pointer_wolfram_es_02.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
