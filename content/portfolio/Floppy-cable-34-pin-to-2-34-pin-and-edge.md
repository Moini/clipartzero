+++
showonlyimage = true
draft = false
image = "img/Floppy_cable_34_pin_to_2-34_pin_and_edge.svg"
date = "2019-01-01T21:12:22+05:30"
title = "Floppy-cable-34-pin-to-2-34-pin-and-edge"
weight = 0
+++

![Right-click to Save image](/img/Floppy_cable_34_pin_to_2-34_pin_and_edge.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
