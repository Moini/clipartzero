+++
showonlyimage = true
draft = false
image = "img/designs-for-tessellated-pavements.svg"
date = "2019-01-01T21:12:22+05:30"
title = "designs-for-tessellated-pavements"
weight = 0
+++

![Right-click to Save image](/img/designs-for-tessellated-pavements.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
