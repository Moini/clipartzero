+++
showonlyimage = true
draft = false
image = "img/Vintage-Santa-Sleigh-And-Reindeer.svg"
date = "2019-01-01T21:12:22+05:30"
title = "Vintage-Santa-Sleigh-And-Reindeer"
weight = 0
+++

![Right-click to Save image](/img/Vintage-Santa-Sleigh-And-Reindeer.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
