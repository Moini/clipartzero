+++
showonlyimage = true
draft = false
image = "img/bird-over-the-sea.svg"
date = "2019-01-01T21:12:22+05:30"
title = "bird-over-the-sea"
weight = 0
+++

![Right-click to Save image](/img/bird-over-the-sea.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
