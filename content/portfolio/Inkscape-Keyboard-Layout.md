+++
showonlyimage = true
draft = false
image = "img/Inkscape_Keyboard_Layout.svg"
date = "2019-01-01T21:12:22+05:30"
title = "Inkscape-Keyboard-Layout"
weight = 0
+++

![Right-click to Save image](/img/Inkscape_Keyboard_Layout.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
