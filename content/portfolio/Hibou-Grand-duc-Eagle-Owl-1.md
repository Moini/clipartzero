+++
showonlyimage = true
draft = false
image = "img/Hibou-Grand-duc-Eagle-Owl-1.svg"
date = "2019-01-01T21:12:22+05:30"
title = "Hibou-Grand-duc-Eagle-Owl-1"
weight = 0
+++

![Right-click to Save image](/img/Hibou-Grand-duc-Eagle-Owl-1.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
