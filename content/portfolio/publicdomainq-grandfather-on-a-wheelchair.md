+++
showonlyimage = true
draft = false
image = "img/publicdomainq-grandfather-on-a-wheelchair.svg"
date = "2019-01-01T21:12:22+05:30"
title = "publicdomainq-grandfather-on-a-wheelchair"
weight = 0
+++

![Right-click to Save image](/img/publicdomainq-grandfather-on-a-wheelchair.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
