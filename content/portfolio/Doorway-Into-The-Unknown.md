+++
showonlyimage = true
draft = false
image = "img/Doorway-Into-The-Unknown.svg"
date = "2019-01-01T21:12:22+05:30"
title = "Doorway-Into-The-Unknown"
weight = 0
+++

![Right-click to Save image](/img/Doorway-Into-The-Unknown.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
