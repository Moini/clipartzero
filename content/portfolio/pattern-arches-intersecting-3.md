+++
showonlyimage = true
draft = false
image = "img/pattern-arches-intersecting-3.svg"
date = "2019-01-01T21:12:22+05:30"
title = "pattern-arches-intersecting-3"
weight = 0
+++

![Right-click to Save image](/img/pattern-arches-intersecting-3.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
