+++
showonlyimage = true
draft = false
image = "img/fire_truck_at_fire_house.svg"
date = "2019-01-01T21:12:22+05:30"
title = "fire-truck-at-fire-house"
weight = 0
+++

![Right-click to Save image](/img/fire_truck_at_fire_house.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
