+++
showonlyimage = true
draft = false
image = "img/Full-Deck-Of-Ornate-Playing-Cards.svg"
date = "2019-01-01T21:12:22+05:30"
title = "Full-Deck-Of-Ornate-Playing-Cards"
weight = 0
+++

![Right-click to Save image](/img/Full-Deck-Of-Ornate-Playing-Cards.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
