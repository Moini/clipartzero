+++
showonlyimage = true
draft = false
image = "img/SteveLambert_Library_Book_Cart.svg"
date = "2019-01-01T21:12:22+05:30"
title = "SteveLambert-Library-Book-Cart"
weight = 0
+++

![Right-click to Save image](/img/SteveLambert_Library_Book_Cart.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
