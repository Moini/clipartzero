+++
showonlyimage = true
draft = false
image = "img/Chromatic-Triangular-Bassel.svg"
date = "2019-01-01T21:12:22+05:30"
title = "Chromatic-Triangular-Bassel"
weight = 0
+++

![Right-click to Save image](/img/Chromatic-Triangular-Bassel.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
