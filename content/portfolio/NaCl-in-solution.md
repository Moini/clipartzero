+++
showonlyimage = true
draft = false
image = "img/NaCl_in_solution.svg"
date = "2019-01-01T21:12:22+05:30"
title = "NaCl-in-solution"
weight = 0
+++

![Right-click to Save image](/img/NaCl_in_solution.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
