+++
showonlyimage = true
draft = false
image = "img/Volcano_simplified_by_Juhele.svg"
date = "2019-01-01T21:12:22+05:30"
title = "Volcano-simplified-by-Juhele"
weight = 0
+++

![Right-click to Save image](/img/Volcano_simplified_by_Juhele.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
