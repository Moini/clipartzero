+++
showonlyimage = true
draft = false
image = "img/editcopy.svg"
date = "2019-01-01T21:12:22+05:30"
title = "editcopy"
weight = 0
+++

![Right-click to Save image](/img/editcopy.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
