+++
showonlyimage = true
draft = false
image = "img/Metal_sword_with_wood_handle.svg"
date = "2019-01-01T21:12:22+05:30"
title = "Metal-sword-with-wood-handle"
weight = 0
+++

![Right-click to Save image](/img/Metal_sword_with_wood_handle.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
