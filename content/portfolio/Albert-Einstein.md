+++
showonlyimage = true
draft = false
image = "img/Albert_Einstein.svg"
date = "2019-01-01T21:12:22+05:30"
title = "Albert-Einstein"
weight = 0
+++

![Right-click to Save image](/img/Albert_Einstein.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
