+++
showonlyimage = true
draft = false
image = "img/Gingerbread-Man-Icon.svg"
date = "2019-01-01T21:12:22+05:30"
title = "Gingerbread-Man-Icon"
weight = 0
+++

![Right-click to Save image](/img/Gingerbread-Man-Icon.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
