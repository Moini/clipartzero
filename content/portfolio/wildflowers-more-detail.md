+++
showonlyimage = true
draft = false
image = "img/wildflowers_more_detail.svg"
date = "2019-01-01T21:12:22+05:30"
title = "wildflowers-more-detail"
weight = 0
+++

![Right-click to Save image](/img/wildflowers_more_detail.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
