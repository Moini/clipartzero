+++
showonlyimage = true
draft = false
image = "img/Shinkansen-N700-Front.svg"
date = "2019-01-01T21:12:22+05:30"
title = "Shinkansen-N700-Front"
weight = 0
+++

![Right-click to Save image](/img/Shinkansen-N700-Front.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
