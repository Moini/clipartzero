+++
showonlyimage = true
draft = false
image = "img/openclipart-coloring-books-2013-03-seasons-holidays-Santa_Panda.svg"
date = "2019-01-01T21:12:22+05:30"
title = "openclipart-coloring-books-2013-03-seasons-holidays-Santa-Panda"
weight = 0
+++

![Right-click to Save image](/img/openclipart-coloring-books-2013-03-seasons-holidays-Santa_Panda.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
