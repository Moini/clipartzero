+++
showonlyimage = true
draft = false
image = "img/Measurement-Block.svg"
date = "2019-01-01T21:12:22+05:30"
title = "Measurement-Block"
weight = 0
+++

![Right-click to Save image](/img/Measurement-Block.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
