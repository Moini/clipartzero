+++
showonlyimage = true
draft = false
image = "img/Supermarket_Seafood_Counter.svg"
date = "2019-01-01T21:12:22+05:30"
title = "Supermarket-Seafood-Counter"
weight = 0
+++

![Right-click to Save image](/img/Supermarket_Seafood_Counter.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
