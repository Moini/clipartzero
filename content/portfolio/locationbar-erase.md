+++
showonlyimage = true
draft = false
image = "img/locationbar_erase.svg"
date = "2019-01-01T21:12:22+05:30"
title = "locationbar-erase"
weight = 0
+++

![Right-click to Save image](/img/locationbar_erase.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
