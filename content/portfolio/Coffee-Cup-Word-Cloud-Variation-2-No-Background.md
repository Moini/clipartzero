+++
showonlyimage = true
draft = false
image = "img/Coffee-Cup-Word-Cloud-Variation-2-No-Background.svg"
date = "2019-01-01T21:12:22+05:30"
title = "Coffee-Cup-Word-Cloud-Variation-2-No-Background"
weight = 0
+++

![Right-click to Save image](/img/Coffee-Cup-Word-Cloud-Variation-2-No-Background.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
