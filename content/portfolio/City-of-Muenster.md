+++
showonlyimage = true
draft = false
image = "img/City_of_Muenster.svg"
date = "2019-01-01T21:12:22+05:30"
title = "City-of-Muenster"
weight = 0
+++

![Right-click to Save image](/img/City_of_Muenster.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
