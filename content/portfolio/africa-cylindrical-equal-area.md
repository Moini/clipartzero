+++
showonlyimage = true
draft = false
image = "img/africa_cylindrical_equal_area.svg"
date = "2019-01-01T21:12:22+05:30"
title = "africa-cylindrical-equal-area"
weight = 0
+++

![Right-click to Save image](/img/africa_cylindrical_equal_area.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
