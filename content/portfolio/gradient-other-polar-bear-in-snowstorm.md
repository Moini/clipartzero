+++
showonlyimage = true
draft = false
image = "img/gradient-other-polar-bear-in-snowstorm.svg"
date = "2019-01-01T21:12:22+05:30"
title = "gradient-other-polar-bear-in-snowstorm"
weight = 0
+++

![Right-click to Save image](/img/gradient-other-polar-bear-in-snowstorm.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
