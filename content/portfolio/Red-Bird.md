+++
showonlyimage = true
draft = false
image = "img/Red_Bird.svg"
date = "2019-01-01T21:12:22+05:30"
title = "Red-Bird"
weight = 0
+++

![Right-click to Save image](/img/Red_Bird.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
