+++
showonlyimage = true
draft = false
image = "img/Stop_Slow_Go_traffic_light.svg"
date = "2019-01-01T21:12:22+05:30"
title = "Stop-Slow-Go-traffic-light"
weight = 0
+++

![Right-click to Save image](/img/Stop_Slow_Go_traffic_light.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
