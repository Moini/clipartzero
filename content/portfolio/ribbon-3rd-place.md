+++
showonlyimage = true
draft = false
image = "img/ribbon_3rd_place.svg"
date = "2019-01-01T21:12:22+05:30"
title = "ribbon-3rd-place"
weight = 0
+++

![Right-click to Save image](/img/ribbon_3rd_place.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
