+++
showonlyimage = true
draft = false
image = "img/Christmas_Tree_bw.svg"
date = "2019-01-01T21:12:22+05:30"
title = "Christmas-Tree-bw"
weight = 0
+++

![Right-click to Save image](/img/Christmas_Tree_bw.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
