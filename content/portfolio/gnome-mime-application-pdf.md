+++
showonlyimage = true
draft = false
image = "img/gnome-mime-application-pdf.svg"
date = "2019-01-01T21:12:22+05:30"
title = "gnome-mime-application-pdf"
weight = 0
+++

![Right-click to Save image](/img/gnome-mime-application-pdf.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
