+++
showonlyimage = true
draft = false
image = "img/Flower_petal_16_daisy.svg"
date = "2019-01-01T21:12:22+05:30"
title = "Flower-petal-16-daisy"
weight = 0
+++

![Right-click to Save image](/img/Flower_petal_16_daisy.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
