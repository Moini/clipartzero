+++
showonlyimage = true
draft = false
image = "img/Palmyra-Temple-Of-BaalShamin.svg"
date = "2019-01-01T21:12:22+05:30"
title = "Palmyra-Temple-Of-BaalShamin"
weight = 0
+++

![Right-click to Save image](/img/Palmyra-Temple-Of-BaalShamin.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
