+++
showonlyimage = true
draft = false
image = "img/11-November_Singing_in_the_Rain.svg"
date = "2019-01-01T21:12:22+05:30"
title = "11-November-Singing-in-the-Rain"
weight = 0
+++

![Right-click to Save image](/img/11-November_Singing_in_the_Rain.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
