+++
showonlyimage = true
draft = false
image = "img/Yellow_Burst_Flower.svg"
date = "2019-01-01T21:12:22+05:30"
title = "Yellow-Burst-Flower"
weight = 0
+++

![Right-click to Save image](/img/Yellow_Burst_Flower.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
