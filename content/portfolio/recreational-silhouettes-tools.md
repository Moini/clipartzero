+++
showonlyimage = true
draft = false
image = "img/recreational_silhouettes-tools.svg"
date = "2019-01-01T21:12:22+05:30"
title = "recreational-silhouettes-tools"
weight = 0
+++

![Right-click to Save image](/img/recreational_silhouettes-tools.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
