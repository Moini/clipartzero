+++
showonlyimage = true
draft = false
image = "img/wreath_of_evergreen_with_red_berries_01.svg"
date = "2019-01-01T21:12:22+05:30"
title = "wreath-of-evergreen-with-red-berries-01"
weight = 0
+++

![Right-click to Save image](/img/wreath_of_evergreen_with_red_berries_01.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
