+++
showonlyimage = true
draft = false
image = "img/tomas-arad-Venus-de-Milo-portrait.svg"
date = "2019-01-01T21:12:22+05:30"
title = "tomas-arad-Venus-de-Milo-portrait"
weight = 0
+++

![Right-click to Save image](/img/tomas-arad-Venus-de-Milo-portrait.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
