+++
showonlyimage = true
draft = false
image = "img/IPS-Logo.svg"
date = "2019-01-01T21:12:22+05:30"
title = "IPS-Logo"
weight = 0
+++

![Right-click to Save image](/img/IPS-Logo.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
