+++
showonlyimage = true
draft = false
image = "img/vintage_camera.svg"
date = "2019-01-01T21:12:22+05:30"
title = "vintage-camera"
weight = 0
+++

![Right-click to Save image](/img/vintage_camera.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
