+++
showonlyimage = true
draft = false
image = "img/folder_cyan_open.svg"
date = "2019-01-01T21:12:22+05:30"
title = "folder-cyan-open"
weight = 0
+++

![Right-click to Save image](/img/folder_cyan_open.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
