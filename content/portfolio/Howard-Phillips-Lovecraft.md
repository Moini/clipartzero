+++
showonlyimage = true
draft = false
image = "img/Howard_Phillips_Lovecraft.svg"
date = "2019-01-01T21:12:22+05:30"
title = "Howard-Phillips-Lovecraft"
weight = 0
+++

![Right-click to Save image](/img/Howard_Phillips_Lovecraft.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
