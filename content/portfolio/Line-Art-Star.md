+++
showonlyimage = true
draft = false
image = "img/Line-Art-Star.svg"
date = "2019-01-01T21:12:22+05:30"
title = "Line-Art-Star"
weight = 0
+++

![Right-click to Save image](/img/Line-Art-Star.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
