+++
showonlyimage = true
draft = false
image = "img/Piece-of-the-Berlin-Wall-at-Mauerpark-2014110934.svg"
date = "2019-01-01T21:12:22+05:30"
title = "Piece-of-the-Berlin-Wall-at-Mauerpark-2014110934"
weight = 0
+++

![Right-click to Save image](/img/Piece-of-the-Berlin-Wall-at-Mauerpark-2014110934.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
