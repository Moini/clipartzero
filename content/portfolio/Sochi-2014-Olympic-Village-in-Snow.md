+++
showonlyimage = true
draft = false
image = "img/Sochi-2014-Olympic-Village-in-Snow.svg"
date = "2019-01-01T21:12:22+05:30"
title = "Sochi-2014-Olympic-Village-in-Snow"
weight = 0
+++

![Right-click to Save image](/img/Sochi-2014-Olympic-Village-in-Snow.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
