+++
showonlyimage = true
draft = false
image = "img/DodgerRed_Flag_Waving.svg"
date = "2019-01-01T21:12:22+05:30"
title = "DodgerRed-Flag-Waving"
weight = 0
+++

![Right-click to Save image](/img/DodgerRed_Flag_Waving.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
