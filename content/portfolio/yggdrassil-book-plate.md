+++
showonlyimage = true
draft = false
image = "img/yggdrassil-book-plate.svg"
date = "2019-01-01T21:12:22+05:30"
title = "yggdrassil-book-plate"
weight = 0
+++

![Right-click to Save image](/img/yggdrassil-book-plate.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
