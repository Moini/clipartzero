+++
showonlyimage = true
draft = false
image = "img/church_building_01_01.svg"
date = "2019-01-01T21:12:22+05:30"
title = "church-building-01-01"
weight = 0
+++

![Right-click to Save image](/img/church_building_01_01.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
