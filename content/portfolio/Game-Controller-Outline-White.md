+++
showonlyimage = true
draft = false
image = "img/Game-Controller-Outline-White.svg"
date = "2019-01-01T21:12:22+05:30"
title = "Game-Controller-Outline-White"
weight = 0
+++

![Right-click to Save image](/img/Game-Controller-Outline-White.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
