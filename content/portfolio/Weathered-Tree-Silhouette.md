+++
showonlyimage = true
draft = false
image = "img/Weathered-Tree-Silhouette.svg"
date = "2019-01-01T21:12:22+05:30"
title = "Weathered-Tree-Silhouette"
weight = 0
+++

![Right-click to Save image](/img/Weathered-Tree-Silhouette.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
