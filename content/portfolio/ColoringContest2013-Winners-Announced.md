+++
showonlyimage = true
draft = false
image = "img/ColoringContest2013-Winners-Announced.svg"
date = "2019-01-01T21:12:22+05:30"
title = "ColoringContest2013-Winners-Announced"
weight = 0
+++

![Right-click to Save image](/img/ColoringContest2013-Winners-Announced.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
