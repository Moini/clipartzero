+++
showonlyimage = true
draft = false
image = "img/the_mad_little_ship_core_01.svg"
date = "2019-01-01T21:12:22+05:30"
title = "the-mad-little-ship-core-01"
weight = 0
+++

![Right-click to Save image](/img/the_mad_little_ship_core_01.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
