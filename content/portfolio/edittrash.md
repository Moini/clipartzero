+++
showonlyimage = true
draft = false
image = "img/edittrash.svg"
date = "2019-01-01T21:12:22+05:30"
title = "edittrash"
weight = 0
+++

![Right-click to Save image](/img/edittrash.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
