+++
showonlyimage = true
draft = false
image = "img/Disco-Crystal-Ball.svg"
date = "2019-01-01T21:12:22+05:30"
title = "Disco-Crystal-Ball"
weight = 0
+++

![Right-click to Save image](/img/Disco-Crystal-Ball.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
