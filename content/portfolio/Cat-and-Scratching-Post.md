+++
showonlyimage = true
draft = false
image = "img/Cat_and_Scratching_Post.svg"
date = "2019-01-01T21:12:22+05:30"
title = "Cat-and-Scratching-Post"
weight = 0
+++

![Right-click to Save image](/img/Cat_and_Scratching_Post.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
