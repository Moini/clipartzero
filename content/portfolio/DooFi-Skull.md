+++
showonlyimage = true
draft = false
image = "img/DooFi_Skull.svg"
date = "2019-01-01T21:12:22+05:30"
title = "DooFi-Skull"
weight = 0
+++

![Right-click to Save image](/img/DooFi_Skull.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
