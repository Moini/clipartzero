+++
showonlyimage = true
draft = false
image = "img/no_camera_sign.svg"
date = "2019-01-01T21:12:22+05:30"
title = "no-camera-sign"
weight = 0
+++

![Right-click to Save image](/img/no_camera_sign.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
