+++
showonlyimage = true
draft = false
image = "img/bicycle_pictogram.svg"
date = "2019-01-01T21:12:22+05:30"
title = "bicycle-pictogram"
weight = 0
+++

![Right-click to Save image](/img/bicycle_pictogram.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
