+++
showonlyimage = true
draft = false
image = "img/Hokusai-Fugaku-36-views-40.svg"
date = "2019-01-01T21:12:22+05:30"
title = "Hokusai-Fugaku-36-views-40"
weight = 0
+++

![Right-click to Save image](/img/Hokusai-Fugaku-36-views-40.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
