+++
showonlyimage = true
draft = false
image = "img/ballot_box_scotland.svg"
date = "2019-01-01T21:12:22+05:30"
title = "ballot-box-scotland"
weight = 0
+++

![Right-click to Save image](/img/ballot_box_scotland.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
