+++
showonlyimage = true
draft = false
image = "img/Vostok-rocket.svg"
date = "2019-01-01T21:12:22+05:30"
title = "Vostok-rocket"
weight = 0
+++

![Right-click to Save image](/img/Vostok-rocket.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
