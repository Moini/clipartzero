+++
showonlyimage = true
draft = false
image = "img/Shakespeare.svg"
date = "2019-01-01T21:12:22+05:30"
title = "Shakespeare"
weight = 0
+++

![Right-click to Save image](/img/Shakespeare.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
