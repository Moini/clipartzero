+++
showonlyimage = true
draft = false
image = "img/Tile_achromatic.svg"
date = "2019-01-01T21:12:22+05:30"
title = "Tile-achromatic"
weight = 0
+++

![Right-click to Save image](/img/Tile_achromatic.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
