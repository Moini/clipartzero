+++
showonlyimage = true
draft = false
image = "img/3D-optical-illusion-frame.svg"
date = "2019-01-01T21:12:22+05:30"
title = "3D-optical-illusion-frame"
weight = 0
+++

![Right-click to Save image](/img/3D-optical-illusion-frame.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
