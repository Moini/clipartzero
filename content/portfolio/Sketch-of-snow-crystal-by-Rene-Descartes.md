+++
showonlyimage = true
draft = false
image = "img/Sketch-of-snow-crystal-by-Rene-Descartes.svg"
date = "2019-01-01T21:12:22+05:30"
title = "Sketch-of-snow-crystal-by-Rene-Descartes"
weight = 0
+++

![Right-click to Save image](/img/Sketch-of-snow-crystal-by-Rene-Descartes.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
