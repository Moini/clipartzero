+++
showonlyimage = true
draft = false
image = "img/Chromatic-Low-Poly-Peace-Hand-Sign.svg"
date = "2019-01-01T21:12:22+05:30"
title = "Chromatic-Low-Poly-Peace-Hand-Sign"
weight = 0
+++

![Right-click to Save image](/img/Chromatic-Low-Poly-Peace-Hand-Sign.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
