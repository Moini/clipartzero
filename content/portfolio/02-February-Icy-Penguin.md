+++
showonlyimage = true
draft = false
image = "img/02-February_Icy_Penguin.svg"
date = "2019-01-01T21:12:22+05:30"
title = "02-February-Icy-Penguin"
weight = 0
+++

![Right-click to Save image](/img/02-February_Icy_Penguin.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
