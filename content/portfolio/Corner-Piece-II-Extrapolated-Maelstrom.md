+++
showonlyimage = true
draft = false
image = "img/Corner-Piece-II-Extrapolated-Maelstrom.svg"
date = "2019-01-01T21:12:22+05:30"
title = "Corner-Piece-II-Extrapolated-Maelstrom"
weight = 0
+++

![Right-click to Save image](/img/Corner-Piece-II-Extrapolated-Maelstrom.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
