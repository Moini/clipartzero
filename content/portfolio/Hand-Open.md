+++
showonlyimage = true
draft = false
image = "img/Hand_Open.svg"
date = "2019-01-01T21:12:22+05:30"
title = "Hand-Open"
weight = 0
+++

![Right-click to Save image](/img/Hand_Open.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
