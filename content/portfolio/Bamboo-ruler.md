+++
showonlyimage = true
draft = false
image = "img/Bamboo_ruler.svg"
date = "2019-01-01T21:12:22+05:30"
title = "Bamboo-ruler"
weight = 0
+++

![Right-click to Save image](/img/Bamboo_ruler.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
