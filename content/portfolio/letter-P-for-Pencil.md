+++
showonlyimage = true
draft = false
image = "img/letter_P_for_Pencil.svg"
date = "2019-01-01T21:12:22+05:30"
title = "letter-P-for-Pencil"
weight = 0
+++

![Right-click to Save image](/img/letter_P_for_Pencil.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
