+++
showonlyimage = true
draft = false
image = "img/Centaur_Dark_Skin.svg"
date = "2019-01-01T21:12:22+05:30"
title = "Centaur-Dark-Skin"
weight = 0
+++

![Right-click to Save image](/img/Centaur_Dark_Skin.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
