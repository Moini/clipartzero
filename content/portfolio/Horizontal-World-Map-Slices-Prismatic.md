+++
showonlyimage = true
draft = false
image = "img/Horizontal-World-Map-Slices-Prismatic.svg"
date = "2019-01-01T21:12:22+05:30"
title = "Horizontal-World-Map-Slices-Prismatic"
weight = 0
+++

![Right-click to Save image](/img/Horizontal-World-Map-Slices-Prismatic.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
