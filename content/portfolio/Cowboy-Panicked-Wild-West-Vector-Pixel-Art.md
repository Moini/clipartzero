+++
showonlyimage = true
draft = false
image = "img/Cowboy_Panicked_Wild_West_Vector_Pixel_Art.svg"
date = "2019-01-01T21:12:22+05:30"
title = "Cowboy-Panicked-Wild-West-Vector-Pixel-Art"
weight = 0
+++

![Right-click to Save image](/img/Cowboy_Panicked_Wild_West_Vector_Pixel_Art.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
