+++
showonlyimage = true
draft = false
image = "img/Tempering_and_Forging_Colors.svg"
date = "2019-01-01T21:12:22+05:30"
title = "Tempering-and-Forging-Colors"
weight = 0
+++

![Right-click to Save image](/img/Tempering_and_Forging_Colors.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
