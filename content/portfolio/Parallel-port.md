+++
showonlyimage = true
draft = false
image = "img/Parallel_port.svg"
date = "2019-01-01T21:12:22+05:30"
title = "Parallel-port"
weight = 0
+++

![Right-click to Save image](/img/Parallel_port.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
