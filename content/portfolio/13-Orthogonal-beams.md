+++
showonlyimage = true
draft = false
image = "img/13_Orthogonal_beams.svg"
date = "2019-01-01T21:12:22+05:30"
title = "13-Orthogonal-beams"
weight = 0
+++

![Right-click to Save image](/img/13_Orthogonal_beams.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
