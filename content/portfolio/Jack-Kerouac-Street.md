+++
showonlyimage = true
draft = false
image = "img/Jack-Kerouac-Street.svg"
date = "2019-01-01T21:12:22+05:30"
title = "Jack-Kerouac-Street"
weight = 0
+++

![Right-click to Save image](/img/Jack-Kerouac-Street.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
