+++
showonlyimage = true
draft = false
image = "img/revert.svg"
date = "2019-01-01T21:12:22+05:30"
title = "revert"
weight = 0
+++

![Right-click to Save image](/img/revert.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
