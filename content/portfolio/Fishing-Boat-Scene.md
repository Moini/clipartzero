+++
showonlyimage = true
draft = false
image = "img/Fishing_Boat_Scene.svg"
date = "2019-01-01T21:12:22+05:30"
title = "Fishing-Boat-Scene"
weight = 0
+++

![Right-click to Save image](/img/Fishing_Boat_Scene.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
