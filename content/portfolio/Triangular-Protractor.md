+++
showonlyimage = true
draft = false
image = "img/Triangular-Protractor.svg"
date = "2019-01-01T21:12:22+05:30"
title = "Triangular-Protractor"
weight = 0
+++

![Right-click to Save image](/img/Triangular-Protractor.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
