+++
showonlyimage = true
draft = false
image = "img/liquid_soap_dispenser.svg"
date = "2019-01-01T21:12:22+05:30"
title = "liquid-soap-dispenser"
weight = 0
+++

![Right-click to Save image](/img/liquid_soap_dispenser.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
