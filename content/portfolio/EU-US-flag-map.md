+++
showonlyimage = true
draft = false
image = "img/EU-US-flag-map.svg"
date = "2019-01-01T21:12:22+05:30"
title = "EU-US-flag-map"
weight = 0
+++

![Right-click to Save image](/img/EU-US-flag-map.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
