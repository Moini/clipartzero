+++
showonlyimage = true
draft = false
image = "img/Kitten-Line-Art-Colored.svg"
date = "2019-01-01T21:12:22+05:30"
title = "Kitten-Line-Art-Colored"
weight = 0
+++

![Right-click to Save image](/img/Kitten-Line-Art-Colored.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
