+++
showonlyimage = true
draft = false
image = "img/ice_cream_cone.svg"
date = "2019-01-01T21:12:22+05:30"
title = "ice-cream-cone"
weight = 0
+++

![Right-click to Save image](/img/ice_cream_cone.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
