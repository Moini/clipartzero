+++
showonlyimage = true
draft = false
image = "img/refugees_welcome_bring_your_families.svg"
date = "2019-01-01T21:12:22+05:30"
title = "refugees-welcome-bring-your-families"
weight = 0
+++

![Right-click to Save image](/img/refugees_welcome_bring_your_families.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
