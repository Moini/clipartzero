+++
showonlyimage = true
draft = false
image = "img/standing_at_the_table.svg"
date = "2019-01-01T21:12:22+05:30"
title = "standing-at-the-table"
weight = 0
+++

![Right-click to Save image](/img/standing_at_the_table.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
