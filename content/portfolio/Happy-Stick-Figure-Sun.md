+++
showonlyimage = true
draft = false
image = "img/Happy_Stick_Figure_Sun.svg"
date = "2019-01-01T21:12:22+05:30"
title = "Happy-Stick-Figure-Sun"
weight = 0
+++

![Right-click to Save image](/img/Happy_Stick_Figure_Sun.svg)

[Creative Commons cc0](https://creativecommons.org/share-your-work/public-domain/cc0)
