+++
date = "2019-11-07T21:12:00+05:30"
title = "About"
+++

All clipart featured here was released by the original creators under a [Creative Commons Zero](https://creativecommons.org/publicdomain/zero/1.0/) license with “no rights reserved.”

As such, it belongs to the public domain. That's you!

What does it all mean?

* You are free to use the clipart for any purpose
* You are free to share or resell the clipart
* You are free to remix the clipart
* No restrictions
* No need to ask for permission
* No requirement to credit the original creator, though it is a nice gesture

